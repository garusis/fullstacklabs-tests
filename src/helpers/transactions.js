/**
 * Created by garusis on 18/06/18.
 */
const knex = require('./knex')

module.exports = function (req, res, next) {
  if (req.method === 'GET') return next()

  const send = res.send
  res.send = function (data) {
    this.send = send
    this.__send_data = data

    if (this.statusCode === 200) {
      return this.trx.commit()
    }
    this.trx.rollback()
  }

  knex
    .transaction(trx => {
      res.trx = req.trx = trx
      next()
    })
    .then(() => res.send(res.__send_data))
    .catch(() => res.send(res.__send_data))
}
