/**
 * Created by garusis on 18/06/18.
 */
const knex = require('knex')
const mockKnex = require('mock-knex')
const knexConfig = require('../../knexfile')

let connection = knex(knexConfig)

if (process.env.NODE_ENV === 'unit-testing') {
  mockKnex.mock(connection)
}

module.exports = connection
