/**
 * Created by garusis on 17/06/18.
 */

const PlayerService = require('../../services/players')

class PlayerController {
  static getList (req, res) {
    return PlayerService.list({team_id: req.team.id})
      .then(list => res.json(list))
      .catch(err => console.log(err) & res.status(500).json(err))
  }

  static loadOne (req, res, next) {
    return PlayerService.getOne({
      team_id: req.team.id,
      id: req.params.player_id
    })
      .then(player => {
        if (!player) return res.status(404).json({})
        req.player = player
        next()
      })
      .catch(err => res.status(500).json(err))
  }

  static getOne (req, res) {
    return res.json(req.player)
  }
}

module.exports = PlayerController
