/**
 * Created by garusis on 18/06/18.
 */
const Joi = require('joi')
const config = require('config')

const cronValidators = {}

cronValidators.fetch = function (req, res, next) {
  const schema = Joi.object().keys({
    token: Joi.string().alphanum().required()
  })

  Joi.validate(req.query, schema, (err, value) => {
    if (err) {
      return res.status(422).json(err.details.map(entry => {
        delete entry.context
        return entry
      }))
    }

    if (value.token !== config.cronToken) {
      return res.status(403).json([{
        message: `"token" ${value.token} is invalid`,
        path: ['token'],
        type: 'credentials.valid'
      }])
    }

    next()
  })
}

module.exports = cronValidators
