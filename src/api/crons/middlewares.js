/**
 * Created by garusis on 17/06/18.
 */
const TeamService = require('../../services/teams')

class TeamController {
  static fetch (req, res) {
    return TeamService.scrapAllTeams()
      .then(scrappedData => TeamService.synchronize(scrappedData, req.trx))
      .then(function (reponse) {
        return res.json(reponse)
      })
      .catch(function (err) {
        return res.status(500).json(err)
      })
  }
}

module.exports = TeamController
