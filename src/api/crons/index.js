/**
 * Created by garusis on 17/06/18.
 */
const express = require('express')
const middlewares = require('./middlewares')
const validators = require('./validators')

const router = express.Router()

// Router for collection endpoints. Get list, create
router.post('/fetch-teams', validators.fetch, middlewares.fetch)

module.exports = router
