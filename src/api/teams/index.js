/**
 * Created by garusis on 17/06/18.
 */
const express = require('express')
const middlewares = require('./middlewares')
const teamPlayerRouter = require('../players/team_players.router')

const collectionRouter = express.Router()
const instanceRouter = express.Router()

// Router for collection endpoints. Get list, create
collectionRouter.get('/', middlewares.getList)

// Router for single instance endponts. Get one, update, remove
collectionRouter.use('/:team_id', middlewares.loadOne, instanceRouter)
instanceRouter.get('/', middlewares.getOne)

/**
 * RELATIONS
 */
// Router for team players collection
instanceRouter.use('/players', teamPlayerRouter)

module.exports = collectionRouter
