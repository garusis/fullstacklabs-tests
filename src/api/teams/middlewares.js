/**
 * Created by garusis on 17/06/18.
 */
const TeamService = require('../../services/teams')

class TeamController {
  static getList (req, res) {
    return TeamService.list()
      .then(list => res.json(list))
      .catch(err => res.status(500).json(err))
  }

  static loadOne (req, res, next) {
    return TeamService.getOne(req.params.team_id)
      .then(team => {
        if (!team) return res.status(404).json({})
        req.team = team
        next()
      })
      .catch(err => res.status(500).json(err))
  }

  static getOne (req, res) {
    return res.json(req.team)
  }
}

module.exports = TeamController
