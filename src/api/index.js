/**
 * Created by garusis on 17/06/18.
 */
const express = require('express')
const teams = require('./teams')
const crons = require('./crons')

const router = express.Router()

router.use('/teams', teams)
router.use('/crons', crons)

module.exports = router
