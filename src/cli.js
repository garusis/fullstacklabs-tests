// noinspection BadExpressionStatementJS
/**
 * Created by garusis on 17/06/18.
 */

const Promise = require('bluebird')

global.Promise = Promise

// eslint-disable-next-line no-unused-expressions
require('yargs')
  .usage('yarn run cli <cmd>')
  .command('fetch-teams', 'Fetch teams stats', () => {
    require('./crons/fetch-teams').fetch()
      .then((results) => console.log(results))
      // .then((results) => console.log(JSON.stringify(results, null, 2)))
      .catch((err) => console.error('err', err))
      .finally(() => process.exit(0))
  })
  .command('gen-cron-token', 'Create a new token for Cron Jobs', () => {
    require('./crons/gen-token').generate()
      .then((results) => console.log(results))
      .catch((err) => console.error(err))
      .finally(() => process.exit(0))
  })
  .showHelpOnFail(true)
  .help()
  .demandCommand()
  .fail((msg, err, yargs) => {
    console.error(yargs.help())
    process.exit(0)
  })
  .argv
