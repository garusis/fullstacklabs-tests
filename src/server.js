/**
 * Created by garusis on 17/06/18.
 */

const express = require('express')
const config = require('config')
const cors = require('cors')
const api = require('./api')
const transactions = require('./helpers/transactions')

const app = express()

app.use(cors())
app.use('/api/v1', transactions, api)

app.listen(config.port, () => app.emit('app::started'))

module.exports = app
