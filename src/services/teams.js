/**
 * Created by garusis on 18/06/18.
 */
const scrapeIt = require('scrape-it')
const _ = require('lodash')
const knex = require('../helpers/knex')
const PlayerService = require('./players')

const wikiUrl = 'https://en.wikipedia.org'

class TeamService {
  static list () {
    return knex('teams')
  }

  static getOne (id) {
    return knex('teams').where('id', id).first()
  }

  static scrapAllTeams () {
    return scrapeIt(`${wikiUrl}/wiki/2018_FIFA_World_Cup`, {
      teams: {
        listItem: 'h3 + .hatnote + .wikitable td:nth-child(2)',
        data: {
          flag: {
            selector: '.flagicon img',
            attr: 'src'
          },
          link: {
            selector: 'a',
            attr: 'href'
          },
          name: {
            selector: 'a',
            how: 'text'
          }
        }
      }
    })
      .then((response) => Promise.all(response.data.teams.map(TeamService.scrapTeam)))
  }

  static scrapTeam (team) {
    return scrapeIt(`${wikiUrl}${team.link}`, {
      mainData: {
        listItem: '.infobox tr',
        data: {
          key: {
            selector: 'th',
            how: 'text'
          },
          value: {
            selector: 'td',
            how: 'text'
          }
        }
      }
    })
      .then(function (response) {
        const data = {}
        const $ = response.$

        response.data.mainData.forEach(item => {
          if (item.key) data[item.key] = item.value
        })

        delete team.link
        team.coach = data['Head coach']
        try {
          team.players = scrapeIt.scrapeHTML($('#Current_squad,#Players').parent().nextAll('table').first().html(), {
            players: {
              listItem: 'tr',
              data: {
                no: {
                  selector: '> *:nth-child(1)',
                  how: 'text'
                },
                position: {
                  selector: '> *:nth-child(2) a',
                  attr: 'title'
                },
                fullname: {
                  selector: '> *:nth-child(3)',
                  how: 'text'
                },
                link: {
                  selector: '> *:nth-child(3) a',
                  attr: 'href'
                },
                date_of_birth: {
                  selector: '> *:nth-child(4) .bday',
                  how: 'text'
                },
                goals: {
                  selector: '> *:nth-child(6)',
                  how: 'text'
                },
                club: {
                  selector: '> *:nth-child(7) a',
                  how: 'text'
                }
              }
            }
          }).players
        } catch (err) {
          console.log(team)
          console.log($('#Current_squad,#Players').parent().nextAll('table').html())
          throw err
        }
        return team
      })
  }

  static synchronize (scrappedData, trx) {
    return knex('teams')
      .select('name', 'id').transacting(trx)
      .then(existingTeams => {
        const teamsToSync = {
          create: [],
          update: []
        }

        scrappedData.forEach(({name, coach, flag}) => {
          const existingTeam = existingTeams.find((team) => team.name === name)

          if (existingTeam) return teamsToSync.update.push({name, coach, flag, id: existingTeam.id})
          teamsToSync.create.push({name, coach, flag})
        })

        return Promise.all([
          knex('teams').transacting(trx).insert(teamsToSync.create).returning(['id', 'name']),
          Promise.all(
            teamsToSync.update.map(team => knex('teams').transacting(trx).where('id', team.id).update(team))
          )
        ])
          .then(([created]) => {
            let sychronizedTeams = Array.isArray(created) ? created.concat(teamsToSync.update) : teamsToSync.update
            sychronizedTeams = _.keyBy(sychronizedTeams, 'name')

            const players = []

            scrappedData.forEach(scrappedTeam => {
              scrappedTeam.players.forEach(scrappedPlayer => {
                scrappedPlayer.team_id = sychronizedTeams[scrappedTeam.name].id
                players.push(scrappedPlayer)
              })
            })
            return players
          })
          .then(scrappedPlayers => PlayerService.synchronize(scrappedPlayers, trx))
          .then(() => Promise.resolve())
      })
  }
}

module.exports = TeamService
