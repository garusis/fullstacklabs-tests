/**
 * Created by garusis on 18/06/18.
 */
const _ = require('lodash')
const knex = require('../helpers/knex')

class PlayerService {
  static list (where) {
    const query = knex('players')
    _.forEach(where, (value, key) => {
      query.where(key, value)
    })
    return query
  }

  static getOne (where) {
    const query = knex('players')
    _.forEach(where, (value, key) => {
      query.where(key, value)
    })
    return query.first()
  }

  static synchronize (scrappedPlayers, trx) {
    scrappedPlayers = scrappedPlayers.filter(player => !isNaN(parseInt(player.no)))
    return knex('players')
      .select('no', 'id', 'team_id').transacting(trx)
      .then(existingPlayers => {
        const playersToSync = {
          create: [],
          update: []
        }

        scrappedPlayers.forEach((scrappedPlayer) => {
          delete scrappedPlayer.link
          const existingPlayer = existingPlayers.find((player) => parseInt(player.no) === parseInt(scrappedPlayer.no) && player.team_id === scrappedPlayer.team_id)
          console.log(scrappedPlayer)
          console.log(existingPlayers)

          if (existingPlayer) return playersToSync.update.push({id: existingPlayer.id, ...scrappedPlayer})
          playersToSync.create.push(scrappedPlayer)
        })

        return Promise.all([
          knex('players').transacting(trx).insert(playersToSync.create),
          Promise.all(
            playersToSync.update.map(player => knex('players').transacting(trx).where('id', player.id).update(player))
          )
        ])
      })
  }
}

module.exports = PlayerService
