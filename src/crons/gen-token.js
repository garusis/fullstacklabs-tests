/**
 * Created by garusis on 17/06/18.
 */
const crypto = require('crypto')
const fs = require('fs')

exports.generate = function () {
  return new Promise((resolve, reject) => {
    let localVars

    try {
      localVars = require('../../config/local.json')
    } catch (err) {
      localVars = {}
    }

    crypto.randomBytes(64, (err, buffer) => {
      if (err) return reject(err)
      localVars.cronToken = buffer.toString('hex')
      fs.writeFile(`${process.cwd()}/config/local.json`, JSON.stringify(localVars), err => err ? reject(err) : resolve('Cron Token created'))
    })
  })
}
