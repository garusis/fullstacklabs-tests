/**
 * Created by garusis on 17/06/18.
 */
const request = require('request-promise')
const config = require('config')

exports.fetch = function () {
  return request({
    method: 'POST',
    uri: `${config.appServer}/api/v1/crons/fetch-teams`,
    qs: {
      token: config.cronToken
    },
    json: true // Automatically stringifies the body to JSON
  })
    .catch(err => Promise.reject(err.error))
}
