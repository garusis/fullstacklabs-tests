exports.up = function (knex) {
  return knex.schema.createTable('players', function (table) {
    table.increments()
    table.string('fullname')
    table.string('no')
    table.date('date_of_birth')
    table.string('place_of_birth')
    table.string('height')
    table.string('position')
    table.string('club')
    table.string('senior_carrier')
    table.string('goals')
    table.integer('team_id').unsigned()
    table.foreign('team_id').references('teams.id')
    table.timestamps(true, true)
  })
}

exports.down = function (knex) {
  return knex.schema.dropTable('players')
}
