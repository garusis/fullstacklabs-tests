/**
 * Created by garusis on 17/06/18.
 */
process.env.NODE_ENV = 'testing'

const config = require('config')
// const supertest = require('supertest')
const chai = require('chai')

global.config = config

/*
 const api = supertest(`${config.get('baseUrl')}/scopeworker-api`);
 const apiAuth = supertest(`${config.get('baseUrl')}/auth/v1`);

 global.api = api;
 global.apiAuth = apiAuth;
 */
global.expect = chai.expect
