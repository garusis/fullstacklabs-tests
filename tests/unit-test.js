/**
 * Created by garusis on 17/06/18.
 */
process.env.NODE_ENV = 'unit-testing'

const config = require('config')
const chai = require('chai')
const tracker = require('mock-knex').getTracker()

global.config = config
global.expect = chai.expect
global.tracker = tracker
