# Fullstacklabs-test-api

This project is the API for the fullstack tests designed by fullstacklabs about 
the World Cup. An implementation of this applications is already deployed in [heroku](https://world-cup-test.herokuapp.com)

## CLI
This project contains a CLI tool to execute commands like cron jobs without to use internal Javascript intervals.
The CLI also have a task to generate a valid authentication token between the CLI and the API.

### Fecth data from wikipedia
This task should be executed every 15 minutes, but that responsibility is delegated to a external tool 
like *crontab* or [Heroku Scheduler](https://devcenter.heroku.com/articles/scheduler) in Heroku. Those tools
are responsible to execute the right command every x minutes.

To execute manually this tasks you must run `npm run cli fetch-teams`

## Migrations
This project use [Knex migrations](https://knexjs.org/#Migrations-CLI) to create the database initial structure.
You just need setup the *$DATABASE_URL* environment variable and then run `knex migrate:latest`.

## Tests
Unfortunately I couldn't create the properly tests, but all the commands and the stack for it is ready.
The project uses *knex-mock* to mock database queries and *nock* to mock HTTP Requests to external resources in unit testings.
To run the test you just need to run `npm test`

## Quality Code
This project use all the style rules described by [Standard.js](https://standardjs.com). To lint the code you can run `npm run lint`.

##Missing items
Unfortunately time wasn't enough to do all items so is missing:
* Websockets communication to notify new changes in the scrapped data.
* Scrap specify information about players.
* Unit Test and Integration test implementation.
